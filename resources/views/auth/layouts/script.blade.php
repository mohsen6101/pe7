<script src="{{ url('auth/js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ url('auth/js/common_scripts.min.js') }}"></script>
<script src="{{ url('auth/js/velocity.min.js') }}"></script>
<script src="{{ url('auth/js/functions.js') }}"></script>
<script src="{{ url('auth/js/survey_func.js') }}"></script>
<script src="{{ url('js/validation.js') }}"></script>
